/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* main.cpp
*
* Combined program for simultaneous multimodal tracking. Opens multiple OpenCV
* windows which show the status of each tracking modality.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#include "eye\EyeVideoTracker.h"
#include "led\LedVideoTracker.h"
#include "openbci\OpenBciTracker.h"

#include <windows.h>

DWORD WINAPI EyeTrackerThread(void* param) {
	EyeVideoTracker* tracker = (EyeVideoTracker*)param;
	tracker->Run();
	return 0;
}

DWORD WINAPI LedTrackerThread(void* param) {
	LedVideoTracker* tracker = (LedVideoTracker*)param;
	tracker->Run();
	return 0;
}

DWORD WINAPI BciTrackerThread(void* param) {
	OpenBciTracker* tracker = (OpenBciTracker*)param;
	tracker->Run();
	return 0;
}

int main() {
	EyeVideoTracker eyetracker;
	LedVideoTracker ledtracker;
	OpenBciTracker bcitracker;

	bcitracker.Connect(true);

	HANDLE handles[3];
	handles[1] = CreateThread(0, 0, LedTrackerThread, &ledtracker, 0, NULL);
	handles[0] = CreateThread(0, 0, EyeTrackerThread, &eyetracker, 0, NULL);
	handles[2] = CreateThread(0, 0, BciTrackerThread, &bcitracker, 0, NULL);

	WaitForMultipleObjects(3, handles, true, INFINITE);

	for (int i = 0; i < 3; i++) {
		CloseHandle(handles[i]);
	}
}