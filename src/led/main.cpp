/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* main.cpp
*
* Program for headset pose tracking using four LEDs positioned on the headset.
* Displays the video feed of the camera used for tracking overlaid with an pose
* estimation graphic.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#include "LedVideoTracker.h"

int main() {
	LedVideoTracker tracker;
	tracker.Run();
}