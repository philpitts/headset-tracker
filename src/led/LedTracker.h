/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* LedTracker.h
*
* Tracks the pose of a headset. Uses four LEDs with known positions to estimate
* the headset pose.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#pragma once

#include <opencv2\core.hpp>

class LedTracker {

public:

	cv::Mat camera;
	cv::Mat distortian;

	LedTracker(cv::Size imageSize);
	void Update(cv::Mat& image);

	cv::Mat GetRotation() { return mRotation; }
	cv::Vec3d GetTranslation() { return mTranslation; }

private:

	double kMinLedArea = 100;		// Smallest area for a blob to be considered an LED
	double kMaxLedArea = 100000;	// Largest area for a blob to be considered an LED

	// Physical positions of the LEDs. Origin and measurement units will
	// correspond with those of the calculated pose.
	const std::vector<cv::Point3f> kObjPoints = {
		cv::Point3f(0.f, 0.f, 0.f),
		cv::Point3f(90.f, 0.f, 0.f),
		cv::Point3f(0.f, -65.f, 0.f),
		cv::Point3f(99.997f, -64.2266f, 0.f)
	};

	// Camera matrix values
	double kCameraMatrixValues[9] = {
		1.5289514601000294e+03,	0.,						3.0849725429503752e+02,
		0.,						1.5637095751601366e+03,	2.4043829306205882e+02,
		0.,						0.,						1.
	};

	// Distortian matrix values
	double kDistortianMatrixValues[5] = {
		-5.8242562218788398e+00,
		5.1579420800538890e+02,
		7.2953141322323915e-03,
		-2.4664958841699881e-02,
		-1.2767029083507587e+04
	};

	bool mTracked;
	cv::Size mImageSize;

	cv::Mat mRotation;
	cv::Vec3d mTranslation;

	std::vector<cv::Point2f> FindLEDs(cv::Mat image);
	void RemoveExtraPoints(std::vector<cv::Point2f>& pts);
	void SortPoints(std::vector<cv::Point2f>& points);
};