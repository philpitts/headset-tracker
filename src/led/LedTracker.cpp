/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* LedTracker.cpp
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#include "LedTracker.h"

#include <opencv2\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\photo.hpp>
#include <opencv2\calib3d.hpp>

using namespace cv;

LedTracker::LedTracker(Size imageSize) {
	mImageSize = imageSize;
	mTracked = false;

	camera = Mat(3, 3, CV_64F, kCameraMatrixValues);
	distortian = Mat(5, 1, CV_64F, kDistortianMatrixValues);
	
	mTranslation = Vec3d(0, 0, 0);
	
	// Set the rotation matrix to identity
	Rodrigues(Mat::eye(3, 3, CV_64F), mRotation);
}

void LedTracker::Update(Mat& image) {
	std::vector<cv::Point2f> leds = FindLEDs(image);

	if (leds.size() != 4) {
		mTracked = false;
		return;
	}

	circle(image, leds[0], 5, CV_RGB(0, 255, 0), 2);
	circle(image, leds[1], 5, CV_RGB(255, 0, 0), 2);
	circle(image, leds[2], 5, CV_RGB(0, 0, 255), 2);
	circle(image, leds[3], 5, CV_RGB(0, 255, 255), 2);

	// Requires exactly four of each object and image points
	solvePnP(kObjPoints, leds, camera, distortian,
		mRotation, mTranslation, true, CV_P3P);
}

std::vector<Point2f> LedTracker::FindLEDs(Mat image) {
	if (image.data == NULL) return std::vector<Point2f>();
	if (image.size() != mImageSize) return std::vector<Point2f>();

	Mat gray;
	cvtColor(~image, gray, CV_RGB2GRAY);
	GaussianBlur(gray, gray, Size(5, 5), 0);

	threshold(gray, gray, 5, 255, THRESH_BINARY);

	std::vector<std::vector<Point> > contours;
	findContours(gray, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

	// Detect the points
	std::vector<Point2f> leds;
	for (size_t i = 0; i < contours.size(); i++) {
		double area = contourArea(contours[i]);
		if (area < kMinLedArea || area > kMaxLedArea) continue;

		Moments m = moments(contours[i]);
		leds.push_back(Point2f((float)(m.m10 / m.m00), (float)(m.m01 / m.m00)));
	}

	if (leds.size() > 3) RemoveExtraPoints(leds);

	SortPoints(leds);
	return leds;
}

void LedTracker::RemoveExtraPoints(std::vector<Point2f>& pts) {

	// Save only the four points which are furthest away from each other

	std::vector<double> distances(pts.size());
	for (size_t i = 0; i < pts.size(); i++) {
		distances[i] = 0;
		for (size_t j = 0; j < pts.size(); j++) {
			if (j == i) continue;
			distances[i] += pow((pts[i].x + pts[j].x), 2) + pow((pts[i].y + pts[j].y), 2);
		}
	}

	struct Comparer {
		Comparer(const std::vector<double>& v) : mV(v) { }
		bool operator ()(size_t a, size_t b) { return mV[a] > mV[b]; }
		const std::vector<double>& mV;
	};

	std::vector<size_t> idxs;
	idxs.resize(distances.size());

	for (size_t i = 0; i < distances.size(); i++) idxs[i] = i;
	std::partial_sort(idxs.begin(), idxs.begin() + 4, idxs.end(), Comparer(distances));

	std::vector<Point2f> save(4);
	for (size_t i = 0; i < 4; i++) {
		save[i] = pts[idxs[i]];
	}
	pts = save;
}

void LedTracker::SortPoints(std::vector<Point2f>& points) {
	if (points.size() != 4) return;

	int idx;
	double min;
	Point2f point;
	
	// Find top left point (index: 0)
	min = DBL_MAX;
	for (int i = 0; i < 4; i++) {
		double distance = pow((0 - points[i].x), 2) + pow((0 - points[i].y), 2);
		if (distance < min) {
			idx = i;
			min = distance;
		}
	}
	point = points[idx];
	points[idx] = points[0];
	points[0] = point;

	// Find top right point (index: 1)
	min = DBL_MAX;
	for (int i = 0; i < 4; i++) {
		double distance = pow((mImageSize.width - points[i].x), 2) + pow((0 - points[i].y), 2);
		if (distance < min) {
			idx = i;
			min = distance;
		}
	}
	point = points[idx];
	points[idx] = points[1];
	points[1] = point;

	// Find bottom left point (index: 2)
	min = DBL_MAX;
	for (int i = 0; i < 4; i++) {
		double distance = pow((0 - points[i].x), 2) + pow((mImageSize.height - points[i].y), 2);
		if (distance < min) {
			idx = i;
			min = distance;
		}
	}
	point = points[idx];
	points[idx] = points[2];
	points[2] = point;

	// Find bottom right point (index: 3)
	min = DBL_MAX;
	for (int i = 0; i < 4; i++) {
		double distance = pow((mImageSize.width - points[i].x), 2) + pow((mImageSize.height - points[i].y), 2);
		if (distance < min) {
			idx = i;
			min = distance;
		}
	}
	point = points[idx];
	points[idx] = points[3];
	points[3] = point;
}