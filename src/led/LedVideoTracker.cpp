/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* LedVideoTracker.cpp
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#include "LedVideoTracker.h"

using namespace cv;

void LedVideoTracker::Run(int cameraIdx) {
	VideoCapture cap = VideoCapture(cameraIdx);
	if (!cap.isOpened()) {
		std::cout << "LedVideoTracker Error: Could not open video feed " << cameraIdx << std::endl;
		return;
	}

	// Give the program time to open the camera feed
	std::this_thread::sleep_for(std::chrono::milliseconds(500));

	namedWindow("LedWindow", CV_WINDOW_AUTOSIZE);

	while (waitKey(30) < 0) {
		Mat frame;
		cap >> frame;

		Size size = frame.size();

		tracker.Update(frame);

		DrawAxis(frame);
		imshow("LedWindow", frame);
	}
}

void LedVideoTracker::DrawAxis(Mat& image) {
	std::vector<Point3f> pts3d = {
		Point3f(0, 0, 0),
		Point3f(10, 0, 0),
		Point3f(0, 10, 0),
		Point3f(0, 0, 10)
	};

	std::vector<Point2f> pts2d(4);
	projectPoints(pts3d, tracker.GetRotation(), tracker.GetTranslation(), tracker.camera, tracker.distortian, pts2d);

	line(image, pts2d[0], pts2d[1], CV_RGB(0, 0, 255), 2);
	line(image, pts2d[0], pts2d[2], CV_RGB(0, 255, 0), 2);
	line(image, pts2d[0], pts2d[3], CV_RGB(255, 0, 0), 2);
}