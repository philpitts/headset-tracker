/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* EyeTracker.h
*
* Tracks an eye pupil. Use multiple frames to improve the accuracy of the
* tracker. Implements the Starburst algorithm (Li and Parkhurst).
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#pragma once

#include <opencv2/core/core.hpp>

class EyeTracker
{

public:

	EyeTracker(cv::Size imageSize);

	// Calculates the pixel which marks the center of the eye in the provided
	// image. If no eye is found the point (-1, -1) is returned.
	cv::Point FindIrisCenter(cv::Mat image);

private:

	const int kSpecularThreshold = 25;					// Max grayscale value for specular reflection
	const int kSpecularDropoffLength = 25;				// Estimated average number of pixels across a specular reflection boundary
	const int kSpecularAreaMin = 1000;					// Minimum area for a blob to be considered specular reflection
	const int kSpecularAreaMax = 10000;					// Maximum area for a blob to be considered specular reflection
	const int kNSpecularBoundarySamples = 26;			// Used for finding the average intensity around a specular reflection boundary
	const int kNSpecularInterpolationMultiplier = 8;	// Used to building a circle mask to remove the specular reflection artifact

	const double kIrisConvergenceThreshold = 3;			// The maximum pixel distance between two estimated iris centers for convergence
	const double kIrisMaxReturnAngleOffset = 0.87266462599716478846184538424431; // Maximum angle to fire search rays from iris boundary
	const double kIrisMarchStep = 1;					// Pixel distance to travel at each step while searching for iris boundary
	const int kNIrisSearchRays = 20;					// Number of search rays to fire when determining an iris center estimate
	const int kMaxNIterations = 100;					// Max number of iris center search iterations to perform
	const int kIrisGradientThreshold = 1;				// The minimum gradient magnitude between two pixels for an iris boundary to be recognized
	const double kIrisMarchWindow = 5;					// Distance along search vector from step point to test for an iris boundary

	cv::Point mLastCenter;
	cv::Size mImageSize;

	void RemoveSpecularReflection(cv::Mat input, cv::Mat& output);
	void FindSpecularBoundingCircles(cv::Mat image, std::vector<cv::Point2f>& centers, std::vector<float>& radii);
	uchar FindBoundaryAverageIntensity(cv::Mat image, cv::Point2f center, float radius);
	void InterpolateCircle(cv::Mat image, cv::Point2f center, float radius);

	cv::Point FindCenter(cv::Mat image);
	bool InsideImage(cv::Point point);
	void ReverseIrisSearch(cv::Mat image, cv::Point feature, float theta, std::vector<cv::Point>& features);
	cv::Point IrisMarchForward(cv::Mat image, cv::Point origin, float angle);
	cv::Point IrisMarchBackward(cv::Mat image, cv::Point origin, float angle);
	cv::Point FindGeometricCenter(std::vector<cv::Point>& features);

	inline double magnitude(cv::Point v) { return sqrt(v.x*v.x + v.y*v.y); }
};