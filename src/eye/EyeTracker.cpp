/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* EyeTracker.cpp
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#define _USE_MATH_DEFINES

#include "EyeTracker.h"

#include <vector>
#include <algorithm>
#include <math.h>

#include <opencv2\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\photo.hpp>

using namespace cv;

EyeTracker::EyeTracker(Size imageSize) {
	mImageSize = imageSize;
	mLastCenter = Point(imageSize.width / 2, imageSize.height / 2);
}

Point EyeTracker::FindIrisCenter(Mat image) {
	if (image.data == NULL) return Point(-1, -1);
	if (image.size() != mImageSize) return Point(-1, -1);

	Mat gray;
	cvtColor(image, gray, CV_RGB2GRAY);
	GaussianBlur(gray, gray, Size(5, 5), 0);

	RemoveSpecularReflection(gray, gray);
	Point center = FindCenter(gray);

	return center;
}

void EyeTracker::RemoveSpecularReflection(Mat input, Mat& output) {
	std::vector<Point2f> centers;
	std::vector<float> radii;

	Mat inv = ~input.clone();

	FindSpecularBoundingCircles(inv, centers, radii);
	
	for (size_t i = 0; i < centers.size(); i++) {
		uchar intensity = FindBoundaryAverageIntensity(inv, centers[i], radii[i]);

		if (!InsideImage(centers[i])) continue;
		inv.at<uchar>(centers[i]) = intensity;
		
		InterpolateCircle(inv, centers[i], radii[i]);
	}

	output.release();
	inv = ~inv;
	inv.copyTo(output);
}

void EyeTracker::FindSpecularBoundingCircles(Mat image, std::vector<Point2f>& centers, std::vector<float>& radii) {
	centers.clear();
	radii.clear();
	
	Mat mask(image.size(), CV_8UC1, Scalar(0));
	circle(mask, Point(mask.cols / 2, mask.rows / 2), 200, Scalar(255), -1);

	Mat roi(image.size(), CV_8UC1, Scalar(0));
	image.copyTo(roi, mask);

	threshold(roi, roi, kSpecularThreshold, 255, THRESH_BINARY);

	std::vector<std::vector<Point> > contours;
	findContours(roi.clone(), contours, RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

	for (size_t i = 0; i < contours.size(); i++) {
		double area = contourArea(contours[i]);
		if (area > kSpecularAreaMin && area < kSpecularAreaMax) {
			float radius;
			Point2f center;

			minEnclosingCircle(contours[i], center, radius);

			centers.push_back(center);
			radii.push_back(radius + kSpecularDropoffLength);
		}
	}
}

uchar EyeTracker::FindBoundaryAverageIntensity(Mat image, Point2f center, float radius) {
	Point2f samplePoint;
	int sumSamples = 0;
	float theta = 0;
	float dTheta = (2 * (float)M_PI) / kNSpecularBoundarySamples;

	for (int i = 0; i < kNSpecularBoundarySamples; i++, theta += dTheta) {
		samplePoint = Point2f(radius * cosf(theta), radius * sinf(theta)) + center;
		if (!InsideImage(samplePoint)) continue;
		sumSamples += image.at<uchar>(samplePoint);
	}

	return sumSamples / kNSpecularBoundarySamples;
}

void EyeTracker::InterpolateCircle(Mat image, Point2f center, float radius) {
	if (!InsideImage(center)) return;
	
	int nLines = (int)(radius * kNSpecularInterpolationMultiplier);
	std::vector<Point2f> points(nLines);

	float theta = 0;
	float dTheta = (2 * (float)M_PI) / nLines;

	for (int i = 0; i < nLines; i++, theta += dTheta) {
		points[i] = Point2f(radius * cosf(theta), radius * sinf(theta)) + center;
	}

	uchar centerI = image.at<uchar>(center);

	for (size_t i = 0; i < points.size(); i++) {
		if (!InsideImage(points[i])) continue;

		LineIterator it(image, center, points[i]);
		uchar borderI = image.at<uchar>(points[i]);

		for (int j = 0; j < it.count; j++, it++) {
			double alpha = double(j) / it.count;
			uchar color = (uchar)(centerI * (1 - alpha) + borderI * alpha);

			image.at<uchar>(it.pos()) = color;
		}
	}
}

Point EyeTracker::FindCenter(Mat image) {
	Point center;
	Point feature;
	std::vector<Point> features;

	float dTheta = (2 * (float)M_PI) / kNIrisSearchRays;

	double change = kIrisConvergenceThreshold;
	for (int n = 0; n < kMaxNIterations && change >= kIrisConvergenceThreshold; n++) {
		features.clear();
		
		float theta = 0.f;
		for (int i = 0; i < kNIrisSearchRays; i++, theta += dTheta) {
			feature = IrisMarchForward(image, mLastCenter, theta);
			
			if (feature != Point(-1, -1)) {
				features.push_back(feature);
				ReverseIrisSearch(image, feature, theta, features);
			}
		}

		center = FindGeometricCenter(features);
		change = magnitude(center - mLastCenter);
		mLastCenter = center;
	}

	return center;
}

void EyeTracker::ReverseIrisSearch(Mat image, Point feature, float theta, std::vector<Point>& features) {
	float dTheta = (float)((2 * kIrisMaxReturnAngleOffset) / kNIrisSearchRays);
	theta += (float)(M_PI - kIrisMaxReturnAngleOffset);
	
	for (int i = 0; i < kNIrisSearchRays; i++, theta += dTheta) {
		feature = IrisMarchBackward(image, mLastCenter, theta);

		if (feature != Point(-1, -1)) features.push_back(feature);
	}
}

bool EyeTracker::InsideImage(Point point) {
	Rect rect(Point(), mImageSize);
	return rect.contains(point);
}

Point EyeTracker::IrisMarchForward(Mat image, Point origin, float angle) {
	int delta = 0;
	int lastDelta = 0;

	Point far;
	Point near = origin;
	Point window = Point2f(cosf(angle), sinf(angle)) * kIrisMarchWindow;
	Point step = Point2f(cosf(angle), sinf(angle)) * kIrisMarchStep;

	// Looking for high magnitude dark -> light gradients
	while (delta <= lastDelta || lastDelta > -kIrisGradientThreshold) {
		lastDelta = delta;
		far = near + window;

		if (!InsideImage(far)) return Point(-1, -1);
		delta = image.at<uchar>(near) - image.at<uchar>(far);

		near += step;
	}
	return (near + window) * 0.5;
}

Point EyeTracker::IrisMarchBackward(Mat image, Point origin, float angle) {
	int delta = 0;
	int lastDelta = 0;

	Point lastPoint;
	Point point = origin;
	Point step = Point2f(cosf(angle), sinf(angle)) * kIrisMarchWindow;

	// Looking for high magnitude light -> dark gradients
	while (delta >= lastDelta || delta < kIrisGradientThreshold) {
		lastDelta = delta;
		lastPoint = point;

		point = lastPoint + step;
		if (!InsideImage(point)) return Point(-1, -1);
		delta = image.at<uchar>(point) - image.at<uchar>(lastPoint);
	}
	return point;
}

Point EyeTracker::FindGeometricCenter(std::vector<Point>& features) {
	long long x = 0;
	long long y = 0;

	if (features.size() == 0) return Point();

	for (size_t i = 0; i < features.size(); i++) {
		x += features[i].x;
		y += features[i].y;
	}

	return Point((int)(x / features.size()), (int)(y / features.size()));
}