/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* EyeVideoTracker.h
*
* Tracks the center of an eye pupil using a camera feed.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#pragma once

#include "EyeTracker.h"

#include <opencv2\opencv.hpp>
#include <iostream>
#include <chrono>
#include <thread>

class EyeVideoTracker {

public:

	EyeVideoTracker(cv::Size videoSize = cv::Size(720, 576)) : tracker(videoSize) { }

	void Run(int cameraIdx = 0);

private:

	EyeTracker tracker;
};