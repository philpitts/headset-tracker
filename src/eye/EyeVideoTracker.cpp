/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* EyeVideoTracker.cpp
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#include "EyeVideoTracker.h"

using namespace cv;

void EyeVideoTracker::Run(int cameraIdx) {
	VideoCapture cap = VideoCapture(cameraIdx);
	if (!cap.isOpened()) {
		std::cout << "VideoTracker Error: Could not open video feed " << cameraIdx << std::endl;
		return;
	}

	// Give the program time to open the camera feed
	std::this_thread::sleep_for(std::chrono::milliseconds(500));

	namedWindow("EyeWindow", CV_WINDOW_AUTOSIZE);

	while (waitKey(30) < 0) {
		Mat frame;
		cap >> frame;

		Size size = frame.size();

		Point center = tracker.FindIrisCenter(frame);
		center.y -= 25;

		circle(frame, center, 10, CV_RGB(0, 255, 0), 2);
		imshow("EyeWindow", frame);
	}
}