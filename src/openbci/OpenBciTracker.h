/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* OpenBciTracker.h
*
* Program for tracking headset gestures using the electrical potentials
* generated from muscle movements. Electrical potentials are measured
* using the OpenBCI microcontroller.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#pragma once

#include "Visualizer.h"
#include "OpenBci.h"
#include "OfflineOpenBci.h"
#include "EyebrowDownClassifier.h"

#include <windows.h>
#include <iostream>

class OpenBciTracker {
	
public:

	OpenBciTracker();
	~OpenBciTracker();

	bool Connect(bool online);
	void Run();

private:

	OpenBci* mSource;
	Visualizer* mViz;

	static DWORD WINAPI StartThread(void* param);

};