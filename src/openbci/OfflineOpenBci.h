/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* OfflineOpenBci.h
*
* Mock OpenBCI class for processing offline data from a file.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#pragma once

#include "OpenBCI.h"

#include <string>
#include <list>
#include <fstream>
#include <mutex>

class OfflineOpenBci : public OpenBci {

public:

	size_t nSamples;

	OfflineOpenBci(size_t nSamples = 50);

	bool Connect(std::string filename) override;
	void Disconnect() override { mFile.close(); }
	bool IsConnected() override { return mFile.is_open(); }

	bool SetChannelSettings() override { return true; }						// stub

	void Run() override;													// Thread safe - call in a new thread
	void Stop() override { Disconnect(); }

	void GetChannelData(std::vector<double>& buffer, int channel) override;	// Thread safe
	void GetAccelerometerData(std::vector<Accelerometer>& buffer) override;	// Thread safe
	double MicrovoltsPerCount(uchar channel) override { return 1.0; }		// stub - already in uV

private:

	std::mutex mMtx;
	std::fstream mFile;

	std::list<double> mChannelData[8];
	std::list<Accelerometer> mAccelerometerData;

	void RemoveNoise(std::vector<double>& buffer);

};