/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* Visualizer.cpp
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#include "Visualizer.h"

using namespace cv;

Visualizer::Visualizer(std::string id, double aScale, size_t aMaxSamples, size_t aUpdateInterval) {
	mID = id;
	maxSamples = aMaxSamples;
	updateInterval = aUpdateInterval;
	scale = aScale;
	mOffset = 0.0;
	mOffsetUpdateCount = 30;

	mGraph = Mat(kGraphSize.height, kGraphSize.width, CV_8UC3, CV_RGB(0, 0, 0));
}

void Visualizer::Show() {
	namedWindow(mID);
	imshow(mID, mGraph);
}

void Visualizer::Hide() {
	cv::destroyWindow(mID);
}

void Visualizer::Update(const std::vector<double>& data) {
	if (!mData.empty()) {
		// Replace stored data with new data as much as possible
		size_t toRemove;
		if (data.size() + mData.size() <= maxSamples) toRemove = 0;
		else if (data.size() >= maxSamples) toRemove = mData.size();
		else toRemove = mData.size() - (maxSamples - data.size());

		for (size_t i = 0; i < toRemove; i++) {
			mData.pop_front();
		}
	}

	// Push samples until the buffer is filled
	for (size_t i = 0; mData.size() < maxSamples && i < data.size(); i++) {
		mData.push_back(data[i]);
	}

	PlotData();
	imshow(mID, mGraph);
}

void Visualizer::PlotData() {
	mGraph = Mat(kGraphSize.height, kGraphSize.width, CV_8UC3, CV_RGB(0, 0, 0));

	if (mData.empty()) return;
	
	double spacer = (double)kGraphSize.width / (maxSamples);
	double middle = (double)kGraphSize.height / 2;

	if (mOffsetUpdateCount++ == updateInterval) {
		double sum = 0.0;
		std::list<double>::iterator it;
		for (it = mData.begin(); it != mData.end(); it++) {
			sum += *it;
		}
		mOffset = sum / mData.size();
		mOffsetUpdateCount = 0;
	}

	double x = 0;
	std::vector<Point> pts(mData.size());
	std::list<double>::iterator it = mData.begin();
	for (size_t i = 0; i < mData.size(); i++, it++) {
		pts[i] = Point((int)x, (int)(middle + scale * (*it - mOffset)));
		x += spacer;
	}

	cv::polylines(mGraph, pts, false, Scalar(0, 255, 0), 1);
}