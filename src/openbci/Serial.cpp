/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* Serial.cpp
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#include "Serial.h"

using namespace std;


bool Serial::Connect(std::string port) {
	if (mConnected) Disconnect();

	m_hSerial = CreateFile(port.c_str(),
		GENERIC_READ | GENERIC_WRITE, 
		0, 
		NULL,
		OPEN_EXISTING,
		0,
		NULL);

	if (m_hSerial == INVALID_HANDLE_VALUE) {
		mError = GetLastError();
		return false;
	}

	DCB dcbSerialParams = { 0 };
	if (!GetCommState(m_hSerial, &dcbSerialParams)) {
		mError = GetLastError();
		return false;
	}

	dcbSerialParams.BaudRate = CBR_115200;
	dcbSerialParams.ByteSize = 8;
	dcbSerialParams.StopBits = ONESTOPBIT;
	dcbSerialParams.Parity = NOPARITY;
	dcbSerialParams.fDtrControl = DTR_CONTROL_ENABLE;

	if (!SetCommState(m_hSerial, &dcbSerialParams)) {
		mError = GetLastError();
		return false;
	}

	mConnected = true;

	PurgeComm(m_hSerial, PURGE_RXCLEAR | PURGE_TXCLEAR);
	Sleep(kConnectionWaitTime);
	return true;
}

void Serial::Disconnect() {
	if (!mConnected) return;

	CloseHandle(m_hSerial);
	mConnected = false;
	return;
}

int Serial::ReadData(uchar* buffer, int size) {
	DWORD bytesRead;

	ClearCommError(m_hSerial, &mError, &mStatus);

	if (mStatus.cbInQue > 0) {
		if (mStatus.cbInQue > (DWORD)size) {
			if (ReadFile(m_hSerial, buffer, size, &bytesRead, NULL)) 
				return bytesRead;
			return -1;
		}
		else {
			if (ReadFile(m_hSerial, buffer, mStatus.cbInQue, &bytesRead, NULL)) 
				return bytesRead;
			return -1;
		}
	}
	return 0;
}

int Serial::WriteData(uchar* buffer, int size) {
	DWORD bytesSent;

	if (!WriteFile(m_hSerial, (void*)buffer, size, &bytesSent, 0)) {
		ClearCommError(m_hSerial, &mError, &mStatus);
		return -1;
	}
	return bytesSent;
}