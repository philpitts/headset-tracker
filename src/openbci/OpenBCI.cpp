/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* OpenBci.cpp
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#include "OpenBci.h"

#include <assert.h>
#include <iostream>

OpenBci::OpenBci() {
	for (uchar i = 0; i < 8; i++) {
		channels[i].powered = true;
		channels[i].gain = 6;
		channels[i].source = 0;
		channels[i].bias = 1;
		channels[i].srb2 = 1;
		channels[i].srb1 = 0;

		mChannelData[i].reserve(kMaxSamples);
	}

	mAccelerometerData.reserve(kMaxSamples);
	mRunning = false;
}

bool OpenBci::Connect(std::string port) {
	if (!mSerial.Connect(port)) return false;

	if (!SendCommand('v')) return false;
	Sleep(1000);
	if (!SendCommand('c')) return false;
	return true;
}

void OpenBci::Disconnect() {
	SendCommand('s');
	return mSerial.Disconnect();
}

bool OpenBci::SetChannelSettings() {
	for (char i = 0; i < 8; i++) {
		if (!SendCommand('x')) return false;
		if (!SendCommand(i + '1')) return false;		
		if (!SendCommand(!channels[i].powered + '0')) return false;
		if (!SendCommand(channels[i].gain + '0')) return false;
		if (!SendCommand(channels[i].source + '0')) return false;
		if (!SendCommand(channels[i].bias + '0')) return false;
		if (!SendCommand(channels[i].srb2 + '0')) return false;
		if (!SendCommand(channels[i].srb1 + '0')) return false;
		if (!SendCommand('X')) return false;
	}
	return true;
}

void OpenBci::Run() {
	if (mRunning) return;

	if (!SendCommand('b')) return;

	mRunning = true;
	while (mSerial.IsConnected() && mRunning) {
		Update();
	}
}

void OpenBci::Stop() {
	if (!mRunning) return;
	if (!SendCommand('s')) return;
	mRunning = false;
}

void OpenBci::GetChannelData(std::vector<double>& buffer, int channel) {
	mMutex.lock();

	buffer = mChannelData[channel];
	mChannelData[channel].clear();
	mChannelData[channel].reserve(kMaxSamples);

	mMutex.unlock();

	RemoveNoise(buffer);
}

void OpenBci::GetAccelerometerData(std::vector<Accelerometer>& buffer) {
	mMutex.lock();

	buffer = mAccelerometerData;
	mAccelerometerData.clear();
	mAccelerometerData.reserve(kMaxSamples);

	mMutex.unlock();
}

double OpenBci::MicrovoltsPerCount(uchar channel) {
	return ((4.5 / kGains[channels[channel].gain]) * 1000000) / (pow(2, 23) - 1);
}

void OpenBci::Update() {
	uchar buffer[33] = { 0 };

	// Read to the head of a packet
	for (int count = 0; buffer[0] != (uchar)0xA0; count++) {
		if (1 != mSerial.ReadData(buffer, 1)) return;
		if (count > 3000) return;
	}

	// Read the rest of the packet
	int count = 1;
	while (count < 33) {
		int read = mSerial.ReadData(&buffer[count], 33 - count);
		if (read == -1) return;
		count += read;
	}

	if (buffer[32] != (uchar)0xC0) return;	// packet corrupted

	// Decode the packet
	Accelerometer accel;
	accel.x = Convert16to32(&buffer[26]);
	accel.y = Convert16to32(&buffer[28]);
	accel.z = Convert16to32(&buffer[30]);

	double channelData[8];
	for (int i = 0; i < 8; i++) {
		channelData[i] = Convert24to32(&buffer[i * 3 + 2]);
	}

	// Update the buffers
	mMutex.lock();

	for (int i = 0; i < 8; i++) {
		if (mChannelData[i].size() < kMaxSamples) {
			mChannelData[i].push_back(channelData[i]);
		}
	}

	mAccelerometerData.push_back(accel);

	mMutex.unlock();
}

int OpenBci::Convert24to32(uchar* buffer) {
	int result = (
		((0xFF & buffer[0]) << 16) |
		((0xFF & buffer[1]) << 8) |
		(0xFF & buffer[2])
	);

	if ((result & 0x00800000) > 0) {
		result |= 0xFF000000;
	}
	else {
		result &= 0x00FFFFFF;
	}

	return result;
}

int OpenBci::Convert16to32(uchar* buffer) {
	int result = (
		((0xFF & buffer[0]) << 8) |
		 (0xFF & buffer[1])
	);

	if ((result & 0x00008000) > 0) {
		result |= 0xFFFF0000;
	}
	else {
		result &= 0x0000FFFF;
	}

	return result;
}

bool OpenBci::SendCommand(uchar cmd) {
	if (1 != mSerial.WriteData(&cmd, 1)) return false;
	Sleep(150);

	if (cmd == 'b') return true;
	uchar c;
	while (1 == mSerial.ReadData(&c, 1)) {
		std::cout << c;
	}

	return true;
}

void OpenBci::RemoveNoise(std::vector<double>& buffer) {
	if (buffer.empty()) return;

	size_t sizewindow = 5;
	if (buffer.size() < sizewindow) return;

	for (size_t i = 0; i < buffer.size(); i++) {
		double sum = 0.0;
		for (size_t j = 0; j < sizewindow; j++) {
			sum += buffer[j];
		}
		buffer[i] = sum / sizewindow;
	}
}