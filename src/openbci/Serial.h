/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* Serial.h
*
* Handles communication across serial ports.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#pragma once

#include <windows.h>
#include <string>

class Serial {

public:

	typedef unsigned char uchar;

	~Serial() { Disconnect(); }

	bool Connect(std::string port);
	void Disconnect();
	bool IsConnected() { return mConnected; }

	int ReadData(uchar* buffer, int size);
	int WriteData(uchar* buffer, int size);

private:

	const int kConnectionWaitTime = 2000;

	bool mConnected;
	HANDLE m_hSerial;
	COMSTAT mStatus;
	DWORD mError;

};