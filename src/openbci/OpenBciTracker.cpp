/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* OpenBciTracker.cpp
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#include "OpenBciTracker.h"

OpenBciTracker::OpenBciTracker() {
	mSource = nullptr;
	mViz = nullptr;
}

OpenBciTracker::~OpenBciTracker() {
	if (mSource) delete mSource;
	if (mViz) delete mViz;
}

bool OpenBciTracker::Connect(bool online) {
	bool connected = false;

	if (online) {
		mSource = new OpenBci();
		connected = mSource->Connect("COM7");
		mViz = new Visualizer("Online Data", .001);
	}
	else {
		mSource = new OfflineOpenBci();
		connected = mSource->Connect("../../../resources/rawdata_clean.txt");
		mViz = new Visualizer("Offline Data", .01);
	}

	if (!connected) {
		delete mSource;
		mSource = nullptr;

		delete mViz;
		mViz = nullptr;
	}

	return connected;
}

void OpenBciTracker::Run() {
	if (!mSource || !mViz) return;

	EyebrowDownClassifier classifier;
	mViz->Show();

	for (int i = 0; i < 8; i++) {
		mSource->channels[i].powered = false;
	}
	mSource->channels[3].powered = true;
	if (!mSource->SetChannelSettings()) return;

	HANDLE hData = CreateThread(0, 0, StartThread, (void*)this, 0, NULL);

	std::vector<double> data;
	while (-1 == cv::waitKey(30)) {

		// Check for eyebrow furrows
		if (classifier.Update(data)) {
			std::cout << "Eyebrow furrow detected!" << std::endl;
		}

		// Update the visualizer and data
		mSource->GetChannelData(data, 3);
		mViz->Update(data);
	}

	WaitForSingleObject(hData, INFINITE);
	CloseHandle(hData);
}

DWORD WINAPI OpenBciTracker::StartThread(void* param) {
	OpenBciTracker* instance = (OpenBciTracker*)param;
	instance->mSource->Run();
	return 0;
}