/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* OfflineOpenBci.cpp
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#include "OfflineOpenBci.h"

#include <sstream>

OfflineOpenBci::OfflineOpenBci(size_t aNSamples) {
	nSamples = aNSamples;
}

bool OfflineOpenBci::Connect(std::string filename) {
	mFile.open(filename.c_str());

	if (mFile.is_open()) return true;
	return false;
}

void OfflineOpenBci::Run() {
	mMtx.lock();

	std::string line;
	while (mFile, std::getline(mFile, line)) {
		if (line[0] == '%') continue;

		std::stringstream ss(line);

		std::string num;
		std::getline(ss, num, ',');		// ignore the first value (sample #)

		for (int i = 0; i < 8; i++) {
			std::getline(ss, num, ',');
			mChannelData[i].push_back(std::stod(num));
		}

		Accelerometer accel;

		std::getline(ss, num, ',');
		accel.x = std::stod(num);

		std::getline(ss, num, ',');
		accel.y = std::stod(num);

		std::getline(ss, num, ',');
		accel.z = std::stod(num);

		mAccelerometerData.push_back(accel);
	}

	mMtx.unlock();
}

void OfflineOpenBci::GetChannelData(std::vector<double>& buffer, int channel) {
	buffer.clear();
	
	mMtx.lock();

	if (!mChannelData[channel].empty()) {
		buffer.resize(nSamples);
		for (size_t i = 0; i < nSamples && !mChannelData[channel].empty(); i++) {
			buffer[i] = mChannelData[channel].front();
			mChannelData[channel].pop_front();
		}
	}

	mMtx.unlock();

	RemoveNoise(buffer);
}

void OfflineOpenBci::GetAccelerometerData(std::vector<Accelerometer>& buffer) {
	mMtx.lock();

	buffer.clear();
	buffer.resize(mAccelerometerData.size());

	std::list<Accelerometer>::iterator it = mAccelerometerData.begin();
	for (size_t i = 0; i < mAccelerometerData.size(); i++, it++) {
		buffer[i] = *it;
	}

	mMtx.unlock();
}

void OfflineOpenBci::RemoveNoise(std::vector<double>& buffer) {
	if (buffer.empty()) return;

	double* dblptr[1];
	dblptr[0] = &buffer[0];

	// Apply a sliding average filter
	// Average every 4 (or 5) samples to smooth out 60hz noise
	int stepsize = 4;
	for (size_t i = 0; i + stepsize < buffer.size(); i++) {
		double sum = 0.0;
		for (int j = 0; j < stepsize; j++) {
			sum += buffer[i + j];
		}
		double mean = sum / stepsize;
		for (int j = 0; j < stepsize; j++) {
			buffer[i + j] = mean;
		}
	}
}