/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* EyebrowDownClassifier.cpp
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#include "EyebrowDownClassifier.h"

#include <assert.h>

EyebrowDownClassifier::EyebrowDownClassifier(size_t aWindow, double aThreshold, size_t length, double variance) {
	window = aWindow;
	threshold = aThreshold;
	eventLength = length;
	eventVariance = variance;

	mNEventSamples = 0;
	mIncreasing = false;
	mDecreasing = false;
}

bool EyebrowDownClassifier::Update(std::vector<double>& emg) {
	size_t i = 0;
	for (; i < emg.size() && mData.size() < window; i++) {
		mData.push_back(emg[i]);
	}

	bool result = false;
	if (mData.size() == window) {
		result = Classify();
		mData.clear();
	}

	for (; i < emg.size() && mData.size() < window; i++) {
		mData.push_back(emg[i]);
	}
	return result;
}

bool EyebrowDownClassifier::Classify() {
	if (window == 0) return false;

	double sum = 0.0;
	for (size_t i = 0; i < mData.size(); i++) {
		sum += mData[i];
	}
	double mean = sum / mData.size();

	for (size_t i = 0; i < mData.size(); i++) {
		if ((mData[i] - mean) > threshold) return true;
	}
	return false;
}