/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* EyebrowDownClassifier.h
*
* Detects eyebrow furrows or sharp downward movements using electrical
* potentials gathered from an EMG.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#pragma once

#include <vector>

class EyebrowDownClassifier {

public:

	size_t window;			// feature sampling window
	double threshold;		// threshold value for derivatives
	size_t eventLength;		// length of entire eyebrow furrow event
	double eventVariance;	// variance of event lengths

	EyebrowDownClassifier(size_t window = 120, double threshold = 500000, size_t eventLength = 45, double variance = 20);

	// Returns true on a sharp eyebrow down movement. This movement is classified 
	// heuristically according to the following set of rules:
	// 1) Signal decreases with negative derivative magnitude >= threshold occurs
	// 2) Signal increases with positive derivative magnitude >= threshold occurs
	// * Entire process occurs within n = length samples
	//
	// Method expects sequential data buffers with size = window
	bool Update(std::vector<double>& emgWindow);

private:

	bool mIncreasing, mDecreasing;
	size_t mNEventSamples;
	std::vector<double> mData;

	bool Classify();

};