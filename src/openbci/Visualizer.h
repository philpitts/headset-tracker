/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* Visualizer.h
*
* Graphicly displays OpenBCI data
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#pragma once

#include "OpenBCI.h"

#include <vector>
#include <list>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

class Visualizer {

public:

	size_t maxSamples;		// Maximum number of samples to display in a frame
	size_t updateInterval;	// Time between frames
	double scale;			// Multiplier used to scale window height

	Visualizer(std::string id, double scale, size_t maxSamples = 500, size_t updateInterval = 30);

	void Show();
	void Hide();
	void Update(const std::vector<double>& data);

private:

	const cv::Size kGraphSize = cv::Size(800, 600);

	cv::Mat mGraph;
	std::string mID;
	double mOffset;
	size_t mOffsetUpdateCount;
	std::list<double> mData;

	void PlotData();
};