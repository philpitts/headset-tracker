/******************************************************************************
* Headset Tracker
* Copyright (C) 2016 Philip Pitts
*
* OpenBci.h
*
* Control class for the OpenBCI 32-bit ChipKit microcontroller
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#pragma once

#include "Serial.h"

#include <windows.h>

#include <string>
#include <mutex>
#include <list>
#include <vector>
#include <atomic>


typedef struct {
	double x, y, z;
} Accelerometer;


class OpenBci {

public:

	typedef unsigned char uchar;

	// Channel Setting Commands
	struct {
		bool powered;	// value =>	--------- Gain ------------
		uchar gain;		// [0-6] => 1 | 2 | 4 | 6 | 8 | 12 | 24
		uchar source;	// ADC channel input source
		bool bias;
		bool srb2;
		bool srb1;
	} channels[8];

	OpenBci();
	~OpenBci() { Disconnect(); }

	virtual bool Connect(std::string port);
	virtual void Disconnect();
	virtual bool IsConnected() { return mSerial.IsConnected(); }

	virtual bool SetChannelSettings();

	virtual void Run();		// Thread safe - call in a new thread
	virtual void Stop();

	virtual void GetChannelData(std::vector<double>& buffer, int channel);	// Thread safe
	virtual void GetAccelerometerData(std::vector<Accelerometer>& buffer);	// Thread safe
	virtual double MicrovoltsPerCount(uchar channel);

private:

	const int kGains[7] = { 1, 2, 4, 6, 8, 12, 24 };	// Gain values
	const size_t kMaxSamples = 10000;					// Maximum number of samples per window

	std::atomic_bool mRunning;
	Serial mSerial;
	std::mutex mMutex;

	std::vector<double> mChannelData[8];
	std::vector<Accelerometer> mAccelerometerData;

	virtual void Update();	// Thread safe

	virtual void RemoveNoise(std::vector<double>& buffer);

	int Convert24to32(uchar* buffer);
	int Convert16to32(uchar* buffer);

	bool SendCommand(uchar cmd);

};